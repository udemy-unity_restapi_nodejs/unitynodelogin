const express = require('express');
const bodyParser = require ('body-parser');
const mysql = require('mysql');
const validator = require('validator');

//#region ############# CONFIG ###############
var port = process.env.PORT || 3000;

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

var connection = mysql.createConnection({
    host: 'db4free.net',
    user: 'marvimm',
    password: 'rh4et0rik!',
    database: 'unitynodevimm',
    port: 3306
    // ssl: true //indicia segurança numa hospedagem mais "concreta"
});

connection.connect(function(err){
    if (err) {
        console.log('connection error: ' + err.stack);
        return;
    }

    console.log('connected as id: ' + connection.threadId);
})

const isWorking = true;
const notWorkingMessage = "Servidor em Manutenção. Por favor, aguarde :)";

//#endregion

//#region ############### GET ############
//Raiz
app.get('/', function(req, res){
    console.log('Passando no: Entrando no GET/ ');
    res.send('Welcome!');
});

//Login
app.get('/login/:email/:password', function (req, res){
    console.log('Passando no: Entrando no GET/LOGIN');

    var erro = false;

    var msg_res = {};
    msg_res.status = isWorking ? 200 : 503;
    msg_res.message = isWorking ? "" : notWorkingMessage;

     var register_temp = {};
     register_temp.email = req.params.email;
     register_temp.password = req.params.password;

     var status_code = 200;
     var msg_text = '';

     console.log(register_temp);

     if (!validator.isEmail(register_temp.email)){
         console.log('Passando no: Login > Validação de Formato de E-mail');
         status_code = 400;
         msg_text = 'E-mail em fomrato inválido :/'
         erro = true;
     }

     if (!erro){
         //Consulta no banco de dados
         //SELECT
        login_select(register_temp).then((result) => {
            console.log('Passando no: Login > login_select.Then() ');

            //Caso não retorne dados compativeis com Email e Senha
            if (parseInt(result.length) == 0){
                console.log('Passando no: Login : login_select.Then() > verifica se resultado == 0');
                status_code = 400;
                msg_text = 'Login ou Senha incorreto, vericais seus dados!';
            }

            //Caso ocorra de conseguir fazer 2 registros com os mesmos dados
            if (parseInt(result.length) > 1){
                console.log('Passando no: Login : login_select.Then() > verifica se resultado > 1');
                status_code = 400;
                msg_text = 'Existe um problema com seus dados, entre em contato com o administrador!';
            }

            //Carregando o Objeto de reposta
            msg_res.status = status_code;
            msg_res.message = msg_text;

            //Retorno da mensagem com status e mensagem
            res.status(msg_res.status).json(msg_res);

        }).catch((err) => {
            console.log('Passando no: Login > login_select.Catch() ');

            if (err) {
                msg_res.status = err.status_code;
                msg_res.message = err.msg_text;
            } else {
                msg_res.status = 500;
                msg_res.message = "Não é possível executar a ação. Tente mais tarde :D";    
            }
            console.log('--->> Login - catch - Erro: ' + msg_res.message);

            //Retorno da mensagem com status e mensagem
            res.status(msg_res.status).json(msg_res);
        });
     } else {
        //Ainda será ajustado ------------****
        msg_res.status = status_code;
        msg_res.message = msg_text;

        res.status(msg_res.status).json(msg_res);
     }

});

//#endregion

//#region ############## POST #############
//Register
app.post('/register', function (req, res){
    console.log('Passando no: Entrando no POST/REGISTER');

    var erro = false;   

     var msg_res = {};
     msg_res.status = 200;
     msg_res.message = "";

     var register_temp = {};
     register_temp = req.body;

     var status_code = 200;
     var msg_text = '';

     console.log(register_temp.email);

     if (!validator.isEmail(register_temp.email)){
         console.log('Passando no: Register > Validação de Formato de E-mail');
         status_code = 400;
         msg_text = 'E-mail em fomrato inválido :/'
         erro = true;
     }

     if (!erro){
        //Consulta no banco de dados
        register_select(register_temp).then((result) => {
            //Verifica se existe e-mail cadastrado
            if (result.length > 0){
                console.log('Passando no: Register -> register_select.Then() -> Verificar resultado > 0');
                status_code = 400;
                msg_text = 'Já existe um cadastro para esse E-mail :(';

                msg_res.status = status_code;
                msg_res.message = msg_text;

                //Retorno da mensagem
                res.status(msg_res.status).json(msg_res);
            } else {
                //Se não existir, faz a inclusão
                register_insert(register_temp).then((insert_result) => {
                    console.log('Passando no: Register -> register_insert.Then() ');

                    msg_res.status = status_code;
                    msg_res.message = msg_text;
    
                    //Retorno da mensagem
                    res.status(msg_res.status).json(msg_res);
                }).catch((insert_err)=>{
                    console.log('Passando no: Register -> register_insert.Catch() ');

                    msg_res.status = insert_err.status_code;
                    msg_res.message = insert_err.msg_text;

                    console.log('Register INSERT - catch - Erro: ' + msg_res.message);
    
                    //Retorno da mensagem
                    res.status(msg_res.status).json(msg_res);
                });
            }

        }).catch((err) => {
            console.log('Passando no: Register > register_catch.Catch() ');

            if (err.status_code) {
                msg_res.status = err.status_code;
                msg_res.message = err.msg_text;
            } else {
                msg_res.status = 500;
                msg_res.message = '--->>> Register - register_select - Catch = Erro disparado no Then... ';
            }

            console.log('--->> Register Select - catch - Erro: ' + msg_res.message);

            //Retorno da mensagem com status e mensagem
            res.status(msg_res.status).json(msg_res);

        });
        
    } else {
        msg_res.status = status_code;
        msg_res.message = msg_text;
    
         res.status(msg_res.status).json(msg_res);
    }

});

//#endregion

//#region ###################### FUNCTIONS ###################

//#### LOGIN
function login_select(register_temp) {
    return new Promise((resolve, reject) => {
        connection.query(`SELECT * FROM login WHERE email = '${register_temp.email}' AND password = '${register_temp.password}' `
        , function(err, results, field){
            var obj_err = {};
            obj_err.msg_text = '--->>> login_select - Não entrou no erro ainda....';

            if (err) {
                console.log('Erro: login_select dentro da PROMISSE: ' + err);
                obj_err.status_code = 400;
                obj_err.msg_text = err;
                reject(obj_err);
            } else {
                console.log('Dentro da Promisse -> Selecionado: ' + results.length);
                resolve(results);
            }
        })
    })
}

//#### REGISTER
function register_select(register_temp){
    return new Promise((resolve, reject) => {
        connection.query(`SELECT * FROM login WHERE email = '${register_temp.email}' `
        , function(err, results, field){
            var obj_err = {};
            obj_err.msg_text = '--->>> register_select - Não entrou no erro ainda....';

            if (err) {
                console.log('Erro: register_select dentro da PROMISSE: ' + err);
                obj_err.status_code = 400;
                obj_err.msg_text = err;
                reject(obj_err);
            } else {
                console.log('Dentro da Promisse -> Selecionado: ' + results.length);
                resolve(results);
            }
        })
    })
}

function register_insert(register_temp){
    return new Promise((resolve, reject) => {
        connection.query(`INSERT INTO login (email, password) VALUES ('${register_temp.email}' , '${register_temp.password}') `
        , function(err, results, field){
            var obj_err = {};
            obj_err.msg_text = '--->>> register_insert - Não entrou no erro ainda....';

            if (err) {
                console.log('Erro: register_insert dentro da PROMISSE: ' + err);
                obj_err.status_code = 400;
                obj_err.msg_text = err;
                reject(obj_err);
            } else {
                console.log('Dentro da Promisse <register_insert> -> Selecionado: ' + results.length +
                 ' | ID: ' + results.insertId );
                resolve(results);
            }
        })
    })
}


//#endregion

app.listen(port, () => {
    console.log(`Listening port: ${port}`)
})